class AddMessageToNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :message, :varchar
  end
end
