class Notification < ApplicationRecord
  include_concern 'Purging'

  belongs_to :notification_type
  belongs_to :initiator, :class_name => "User", :foreign_key => 'user_id'
  belongs_to :subject, :polymorphic => true
  belongs_to :cause, :polymorphic => true
  has_many :notification_recipients, :dependent => :delete_all
  has_many :recipients, :class_name => "User", :through => :notification_recipients, :source => :user

  accepts_nested_attributes_for :notification_recipients
  before_create :set_notification_recipients
  # Do not emit notifications if they are not enabled for the server
  after_commit :emit_message, :on => :create

  before_save :backup_subject_name

  serialize :options, Hash
  default_value_for(:options) { Hash.new }

  scope :of_type, ->(notification_type) { joins(:notification_type).where(:notification_types => {:name => notification_type}) }

  def type=(typ)
    self.notification_type = NotificationType.find_by!(:name => typ)
  end

  def self.emit_for_event(event)
    omit_event_type = ["VM_CONSOLE_CONNECTED", "VM_SET_TICKET", "VM_CONSOLE_DISCONNECTED", "ems_auth_valid"]
    return if omit_event_type.include?(event.event_type.to_s)

    if NotificationType.names.include?(event.event_type) then
      type = NotificationType.find_by(:name => event.event_type)
      return unless type.enabled?

      return Notification.create(:notification_type => type,
                          :options           => event.full_data,
                          :subject           => event.target)
    end

    event_desc = ''
    provider_arch = ''
    provider_type = ''
    if event.ext_management_system.nil? then
      if !event.target.nil? then
        if event.target.class.name.include? 'ManageIQ::Providers::Openstack::CloudManager' then
          if event.target.ext_management_system.type.to_s == "ManageIQ::Providers::Openstack::CloudManager" then
            event_desc = event.event_type.downcase.gsub('_', ' ')
            #provider_arch = 'clouds'
            #provider_type = 'openstack'
          end
        elsif event.target.class.name.include? "ManageIQ::Providers::Amazon::CloudManager" then
          if event.target.ext_management_system.type.to_s == "ManageIQ::Providers::Amazon::CloudManager" then
            event_desc = event.event_type.downcase.gsub('_', ' ')
            #provider_arch = 'clouds'
            #provider_type = 'Amazon'
          end
        end
      end
    else
      if event.ext_management_system.type == 'ManageIQ::Providers::Redhat::InfraManager' then
        return if event.event_type != event.event_type.upcase

        event_desc = event.event_type.downcase.gsub('_', ' ')
        provider_arch = 'infra'
        provider_type = 'rhev'
      elsif event.ext_management_system.type == "ManageIQ::Providers::Vmware::InfraManager" then
        event_desc = event.event_type.scan(/[A-Z][a-z]+/).join(' ').downcase.gsub('_', '')
        #provider_arch = 'infra'
        #provider_type = 'vmware'
      end
    end

    if event_desc != '' && provider_arch != '' && provider_type != ''
      type = NotificationType.find_by(:name => "ems_valid_event")

      event.vm_or_template.refresh_ems if !event.vm_or_template.nil?
      if !event.target.nil?
        event.target.refresh_ems if event.target.class.name == "ManageIQ::Providers::Amazon::CloudManager::Vm"
      end

      #message = "#{event_desc} ("
      message = "#{event.message} ("
      message += "target: #{event.target}   " if !event.target.nil?
      #message += "vm_name: #{event.vm_or_template}   " if !event.vm_or_template.nil?
      #message += "host_name: #{event.host}   " if !event.host.nil?
      message += "provider_type: #{provider_arch}-#{provider_type}   provider_name: #{event.target.ext_management_system.name}" if !event.target.nil?
      message += "provider_type: #{provider_arch}-#{provider_type}   provider_name: #{event.ext_management_system}" if !event.ext_management_system.nil?
      message += ")"

      Notification.create(:notification_type => type,
                          :message           => message)
    end
  end

  def to_h
    {
      :level      => notification_type.level,
      :created_at => created_at,
      :text       => message ? message : notification_type.message,
      :bindings   => text_bindings,
      :message    => message
    }
  end

  def seen_by_all_recipients?
    notification_recipients.unseen.empty?
  end

  def self.notification_text(name, message_params)
    return unless message_params && NotificationType.names.include?(name)
    type = NotificationType.find_by(:name => name)
    type.message % message_params
  end

  private

  def emit_message
    return unless ::Settings.server.asynchronous_notifications
    notification_recipients.pluck(:id, :user_id).each do |id, user|
      ActionCable.server.broadcast("notifications_#{user}", to_h.merge(:id => id.to_s))
    end
  end

  def set_notification_recipients
    subscribers = notification_type.subscriber_ids(subject, initiator)
    if subject
      subscribers.reject! do |subscriber_id|
        Rbac.filtered_object(subject, :user => User.find(subscriber_id)).blank?
      end
    end
    self.notification_recipients_attributes = subscribers.collect { |id| {:user_id => id } }
  end

  def backup_subject_name
    return unless subject
    backup_name = (subject.try(:name) || subject.try(:description))

    # Note, options are read in text_bindings_dynamic and used in text_bindings
    # if the subject is no longer there such as when a vm is deleted.
    self.options[:subject] = backup_name if backup_name
  end

  def text_bindings
    [:initiator, :subject, :cause].each_with_object(text_bindings_dynamic) do |key, result|
      value = public_send(key)
      next unless value

      # Set the link based on the notification_type.link_to
      result[:link] = {
        :id    => value.id,
        :model => value.class.name
      } if notification_type.link_to.try(:to_sym) == key

      result[key] = {
        :text => value.try(:name) || value.try(:description)
      }
    end
  end

  def text_bindings_dynamic
    options.each_with_object({}) do |(key, value), result|
      result[key] = {
        :text => value
      }
    end
  end
end
